# SPDX-License-Identifier: GPL-2.0-or-later
"""
Web Hook handlers.

To add a handler for a new event type, add a new function to the module and
register it to the :data:`WEBHOOKS` dictionary.
"""
import json
import logging

from django import http
from django.conf import settings
from django.views.decorators import csrf, http as http_decorators

import gitlab as gitlab_module


_log = logging.getLogger(__name__)

CONFIG_LABEL = "Configuration"


@http_decorators.require_POST
@csrf.csrf_exempt
def web_hook(request: http.HttpRequest) -> http.HttpResponse:
    """
    Generic web hook handler for GitLab

    This is responsible for checking the authenticity of the request and
    dispatching it to the proper function based on the X-Gitlab-Event header.
    """
    try:
        secret = request.headers["X-Gitlab-Token"]
    except KeyError:
        return http.HttpResponseForbidden("Permission denied: missing web hook token")

    if secret != settings.FKW_GITLAB_WEBHOOK_SECRET:
        return http.HttpResponseForbidden("Permission denied: invalid web hook token")

    try:
        payload = json.loads(request.body, encoding=request.encoding)
    except json.JSONDecodeError:
        return http.HttpResponseBadRequest("JSON body required in POST")

    try:
        event_type = request.headers["X-Gitlab-Event"]
    except KeyError:
        return http.HttpResponseBadRequest('Missing "X-Gitlab-Event" header in request')

    try:
        handler = WEBHOOKS[event_type]
    except KeyError:
        _log.error(
            "No web hook handler for '%s' events; Adjust your web hooks on GitLab",
            request.headers["X-Gitlab-Event"],
        )
        return http.HttpResponseBadRequest("No web hook handler for request")

    _log.info("Handling web hook request with the '%s' handler", event_type)

    gitlab = gitlab_module.Gitlab(
        settings.FKW_GITLAB_URL, private_token=settings.FKW_GITLAB_TOKEN,
    )
    return handler(payload, gitlab)


def merge_request(payload: dict, gitlab: gitlab_module.Gitlab) -> http.HttpResponse:
    """
    Web hook for when a new merge request is created, an existing merge request
    was updated/merged/closed or a commit is added in the source branch

    The request body is a JSON document documented at
    https://docs.gitlab.com/ce/user/project/integrations/webhooks.html
    """
    project = gitlab.projects.get(payload["project"]["id"])
    merge_request = project.mergerequests.get(payload["object_attributes"]["iid"])

    _apply_config_label(merge_request)
    return http.HttpResponse("Success!")


def _apply_config_label(merge_request) -> None:
    """Add or remove the CONFIG_LABEL to merge requests."""
    config_dirs = [f"redhat/configs/{flavor}" for flavor in ("fedora", "common", "ark")]
    for change in merge_request.changes()["changes"]:
        for config_dir in config_dirs:
            if change["old_path"].startswith(config_dir) or change[
                "new_path"
            ].startswith(config_dir):
                merge_request.labels.append(CONFIG_LABEL)
                merge_request.save()
                return

    # Nothing touches the configuration so ensure the label is *not* applied.
    if CONFIG_LABEL in merge_request.labels:
        merge_request.labels = [
            label for label in merge_request.labels if label != CONFIG_LABEL
        ]
        merge_request.save()


# The set of web hook handlers that are currently supported. Consult `Gitlab
# webhooks`_ documentation for complete list of webhooks and the payload
# details.
#
# The key should be the value GitLab uses in the "X-Gitlab-Event" header of
# the web hook request.
#
# .. _Gitlab webhooks:
# https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
WEBHOOKS = {
    "Merge Request Hook": merge_request,
}
