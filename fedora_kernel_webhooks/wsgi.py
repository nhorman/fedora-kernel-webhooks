# SPDX-License-Identifier: GPL-2.0-or-later
"""
WSGI config for fedora_kernel_webhooks project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fedora_kernel_webhooks.settings")

application = get_wsgi_application()
