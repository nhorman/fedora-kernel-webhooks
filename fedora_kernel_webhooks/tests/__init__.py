# SPDX-License-Identifier: GPL-2.0-or-later
import os

from django.test import TestCase
import vcr


TEST_ROOT = os.path.dirname(os.path.realpath(__file__))
FIXTURES = os.path.join(TEST_ROOT, "fixtures")


class VcrTestCase(TestCase):
    """Record HTTP requests and responses for replay."""

    def setUp(self):
        my_vcr = vcr.VCR(
            cassette_library_dir=os.path.join(FIXTURES, "VCR/"), record_mode="once"
        )
        self.vcr = my_vcr.use_cassette(self.id())
        self.vcr.__enter__()
        self.addCleanup(self.vcr.__exit__, None, None, None)
