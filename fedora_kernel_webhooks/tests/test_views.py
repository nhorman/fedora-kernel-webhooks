# SPDX-License-Identifier: GPL-2.0-or-later

from unittest import mock

from django import http
from django.conf import settings
from django.test import TestCase, Client
from django.urls import reverse

from .. import views


class WebHookTestCase(TestCase):
    """Tests for :func:`fedorak_kernel_webhooks.views.web_hook`."""

    def test_missing_token(self):
        """Failing to provide a token should return a HTTP 403."""
        client = Client()
        response = client.post(reverse("fkw-webhooks"))

        self.assertEqual(403, response.status_code)
        self.assertEqual(b"Permission denied: missing web hook token", response.content)

    def test_invalid_token(self):
        """If the token doesn't match the configured secret, the endpoint should return 403"""
        client = Client()
        response = client.post(
            reverse("fkw-webhooks"), HTTP_X_GITLAB_TOKEN="notavalidtoken"
        )

        self.assertEqual(403, response.status_code)
        self.assertEqual(b"Permission denied: invalid web hook token", response.content)

    def test_invalid_payload(self):
        """Missing JSON payload should return HTTP 400."""
        client = Client()
        response = client.post(
            reverse("fkw-webhooks"),
            HTTP_X_GITLAB_TOKEN=settings.FKW_GITLAB_WEBHOOK_SECRET,
        )

        self.assertEqual(400, response.status_code)
        self.assertEqual(b"JSON body required in POST", response.content)

    def test_missing_event_type(self):
        """Missing event type should return HTTP 400."""
        client = Client()
        response = client.post(
            reverse("fkw-webhooks"),
            data={},
            content_type="application/json",
            HTTP_X_GITLAB_TOKEN=settings.FKW_GITLAB_WEBHOOK_SECRET,
        )

        self.assertEqual(400, response.status_code)
        self.assertEqual(
            b'Missing "X-Gitlab-Event" header in request', response.content
        )

    def test_invalid_event_type(self):
        """Event type with no handler should return HTTP 400."""
        client = Client()
        response = client.post(
            reverse("fkw-webhooks"),
            data={},
            content_type="application/json",
            HTTP_X_GITLAB_TOKEN=settings.FKW_GITLAB_WEBHOOK_SECRET,
            HTTP_X_GITLAB_EVENT="Not a Real Event",
        )

        self.assertEqual(400, response.status_code)
        self.assertEqual(b"No web hook handler for request", response.content)

    def test_valid_webhook(self):
        """Assert the request is forwarded to a handler if all goes well."""
        client = Client()
        with mock.patch.dict(
            views.WEBHOOKS,
            {"Test Event": lambda payload, gitlab: http.HttpResponse("Yay!")},
        ):
            response = client.post(
                reverse("fkw-webhooks"),
                data={},
                content_type="application/json",
                HTTP_X_GITLAB_TOKEN=settings.FKW_GITLAB_WEBHOOK_SECRET,
                HTTP_X_GITLAB_EVENT="Test Event",
            )

        self.assertEqual(200, response.status_code)
        self.assertEqual(b"Yay!", response.content)


class ApplyConfigLabelTestCase(TestCase):
    def test_no_config_change(self):
        """
        Assert merge requests that don't touch redhat/configs/{fedora,common,ark}
        are untouched.
        """
        merge_request = mock.Mock()
        merge_request.labels = []
        merge_request.changes.return_value = {
            "changes": [
                {
                    "new_path": "redhat/configs/build_configs.sh",
                    "old_path": "redhat/configs/build_configs.sh",
                }
            ]
        }

        views._apply_config_label(merge_request)

        self.assertEqual(0, merge_request.save.call_count)

    def test_fedora_config_change(self):
        """Assert merge requests that touch redhat/configs/fedora are tagged."""
        merge_request = mock.Mock()
        merge_request.labels = []
        merge_request.changes.return_value = {
            "changes": [
                {
                    "new_path": "redhat/configs/fedora/CONFIG_THING",
                    "old_path": "redhat/configs/fedora/CONFIG_THING",
                }
            ]
        }

        views._apply_config_label(merge_request)

        self.assertEqual(1, merge_request.save.call_count)
        self.assertEqual([views.CONFIG_LABEL], merge_request.labels)

    def test_common_config_change(self):
        """Assert merge requests that touch redhat/configs/common are tagged."""
        merge_request = mock.Mock()
        merge_request.labels = []
        merge_request.changes.return_value = {
            "changes": [
                {
                    "new_path": "redhat/configs/common/CONFIG_THING",
                    "old_path": "redhat/configs/common/CONFIG_THING",
                }
            ]
        }

        views._apply_config_label(merge_request)

        self.assertEqual(1, merge_request.save.call_count)
        self.assertEqual([views.CONFIG_LABEL], merge_request.labels)

    def test_ark_config_change(self):
        """Assert merge requests that touch redhat/configs/ark are tagged."""
        merge_request = mock.Mock()
        merge_request.labels = []
        merge_request.changes.return_value = {
            "changes": [
                {
                    "new_path": "redhat/configs/ark/CONFIG_THING",
                    "old_path": "redhat/configs/ark/CONFIG_THING",
                }
            ]
        }

        views._apply_config_label(merge_request)

        self.assertEqual(1, merge_request.save.call_count)
        self.assertEqual([views.CONFIG_LABEL], merge_request.labels)

    def test_remove_label(self):
        """Assert merge requests that touch redhat/configs/ark are tagged."""
        merge_request = mock.Mock()
        merge_request.labels = [views.CONFIG_LABEL]
        merge_request.changes.return_value = {
            "changes": [{"new_path": "kernel/cpu.c", "old_path": "kernel/cpu.c"}]
        }

        views._apply_config_label(merge_request)

        self.assertEqual(1, merge_request.save.call_count)
        self.assertEqual([], merge_request.labels)
